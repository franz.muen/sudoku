# pydoku

---

⚠️ Pre-Alpha ⚠️

(not yet) a sudoku solver and generator written in python.

---

## Installation

```
pip install -r requirements.txt
git clone https://git.kkg.berlin/franz.muen/sudoku
cd sudoku
python3 main.py
```

---

## Usage

The script can't be run from the command line yet. The input has to be given in-file.

---

## Contribution

I'm releasing this now pretty much only to get feedback so pull requests are VERY welcome.

Escpecially welcome are ideas on how one could generate a human-solvable sudoku.

---

## License

Licensed under GNU General Public License v3  
You can do anything with this code as long as you keep it open source.
