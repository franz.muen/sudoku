from numpy import empty, int8
from requests import get
import json

tensor = empty((10, 9, 9), dtype=int8)

# gets a test input from api
def get_input():
    r = get("https://sugoku.onrender.com/board?difficulty=easy")
    input_board = json.loads(r.text)["board"]
    for i in range(len(input_board)):
        for j in range(len(input_board[0])):
            tensor[0][i][j] = input_board[i][j]
    for k in range(len(input_board)):
        print(input_board[k])


# gets possible candidates for every empty field
def get_candidates():
    for y in range(len(tensor[0])):
        for x in range(len(tensor[0][y])):
            if tensor[0][y][x] == 0:
                for z in range(1, len(tensor)):
                    tensor[z][y][x] = 0
                possibles = [1, 2, 3, 4, 5, 6, 7, 8, 9]
                to_remove = []
                for candidate in possibles:
                    # checks row for candidate
                    for i in range(len(tensor[0][y])):
                        if candidate == tensor[0][y][i] and candidate not in to_remove:
                            to_remove.append(candidate)
                    # checks column for candidate
                    for k in range(len(tensor[0])):
                        if candidate == tensor[0][k][x] and candidate not in to_remove:
                            to_remove.append(candidate)
                    # checks section for candidate
                    section_coords = ((x // 3) * 3, (y // 3) * 3)
                    for l in range(section_coords[1], section_coords[1] + 3):
                        for m in range(section_coords[0], section_coords[0] + 3):
                            if candidate == tensor[0][l][m] and candidate not in to_remove:
                                to_remove.append(candidate)
                for trash in to_remove:
                    possibles.remove(trash)
                for z in range(len(possibles)):
                    tensor[z + 1][y][x] = possibles[z]


# searches for candidates that can be safely filled in
def fill_board():
    for y in range(len(tensor[0])):
        for x in range(len(tensor[0][y])):
            # gets candidates for field
            candidates = []
            for z in range(1, len(tensor)):
                if tensor[z][y][x] != 0:
                    candidates.append(tensor[z][y][x])
            # checks if there is only one candidate
            if len(candidates) == 1:
                tensor[0][y][x] = candidates[0]
                get_candidates()
            # checks if a candidate only exists once in row
            for candidate in candidates:
                row_candidates = []
                for i in range(len(tensor[0][y])):
                    if i != x:
                        for j in range(1, len(tensor)):
                            row_candidates.append(tensor[j][y][i])
                if candidate not in row_candidates:
                    tensor[0][y][x] = candidate
                    get_candidates()
            # check if candidate only exists once in column
            for candidate in candidates:
                column_candidates = []
                for k in range(len(tensor[0])):
                    if k != y:
                        for l in range(1, len(tensor)):
                            column_candidates.append(tensor[l][k][x])
                if candidate not in column_candidates:
                    tensor[0][y][x] = candidate
                    get_candidates()
            # checks if candidate only exists once in section
            for candidate in candidates:
                section_candidates = []
                section_coords = ((x // 3) * 3, (y // 3) * 3)
                for m in range(section_coords[1], section_coords[1] + 3):
                    for n in range(section_coords[0], section_coords[0] + 3):
                        if tensor[0][y][x] != tensor[0][m][n]:
                            for o in range(1, len(tensor)):
                                section_candidates.append(tensor[o][m][n])
                if candidate not in section_candidates:
                    tensor[0][y][x] = candidate
                    get_candidates()


# checks if board is full
def check_board():
    for y in range(len(tensor[0])):
        for x in range(len(tensor[0][y])):
            if tensor[0][y][x] == 0: 
                return False
    return True


get_input()
get_candidates()
for a in range(50):
    fill_board()
print(tensor)